import { browser } from '$app/environment';
import type { Options } from '../stores/options';
import type { AuthenticationData } from './backends/rss.types';

const saveKey = 'svelte-rss';
const authDataKey = 'authData';
const optionsKey = 'options';

type SavedData = {
	[authDataKey]?: AuthenticationData;
	[optionsKey]?: Options;
};

export const saveAuthData = (params: AuthenticationData) => save(authDataKey, params);
export const saveOptions = (params: Options) => save(optionsKey, params);

export const loadAuthData = (): AuthenticationData | undefined => load()[authDataKey];
export const loadOptionsData = (): Options | undefined => load()[optionsKey];

const save = (key: 'authData' | 'options', data: AuthenticationData | Options) => {
	if (!browser) {
		return;
	}

	const existingData = load();
	switch (key) {
		case authDataKey:
			existingData[authDataKey] = data as AuthenticationData;
			break;
		case optionsKey:
			existingData[optionsKey] = data as Options;
			break;
	}
	localStorage.setItem(saveKey, JSON.stringify(existingData));
};

const load = (): SavedData => {
	if (!browser) {
		return {};
	}

	const loadedData = localStorage.getItem(saveKey);
	if (loadedData) {
		return JSON.parse(loadedData);
	}

	return {};
};
