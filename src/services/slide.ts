import { spring } from 'svelte/motion';

// Inspired by https://svelte.dev/repl/f649d828badf468cacad8bdc8b4088a3?version=3.29.0

export const slideCoords = spring(
	{ x: 0, y: 0 },
	{
		stiffness: 0.05,
		damping: 0.5,
	},
);

type SlideParams = {
	onSlideStart?: () => void;
	onSlideMove?: () => void;
	onSlideEnd?: () => void;
	trackMouse?: boolean;
	minSlideDelta?: number;
};

export function slide(
	node: HTMLElement,
	{ onSlideStart, onSlideMove, onSlideEnd, trackMouse = false, minSlideDelta = 0 }: SlideParams,
) {
	let x = 0;
	let y = 0;
	let slidingXDelta = 0;

	function handleMousedown(event: MouseEvent | TouchEvent) {
		if (event.type.startsWith('touch')) {
			x = (event as TouchEvent).touches[0].pageX;
			y = (event as TouchEvent).touches[0].pageY;
		} else {
			y = (event as MouseEvent).clientY;
			x = (event as MouseEvent).clientX;
		}

		slideCoords.stiffness = 1;
		slideCoords.damping = 1;
		slideCoords.set({ x: 0, y: 0 }, { hard: true });

		onSlideStart && onSlideStart();

		window.addEventListener('touchmove', handleMousemove);
		window.addEventListener('touchend', handleMouseup);

		if (trackMouse) {
			window.addEventListener('mousemove', handleMousemove);
			window.addEventListener('mouseup', handleMouseup);
		}
	}

	function handleMousemove(event: MouseEvent | TouchEvent) {
		let clientX = x;
		let clientY = y;

		if (event.type.startsWith('touch')) {
			clientX = (event as TouchEvent).touches[0].pageX;
			clientY = (event as TouchEvent).touches[0].pageY;
		} else {
			clientX = (event as MouseEvent).clientX;
			clientY = (event as MouseEvent).clientY;
		}
		const dx = clientX - x;
		const dy = clientY - y;
		x = clientX;
		y = clientY;
		slidingXDelta += dx;

		if (slidingXDelta > minSlideDelta) {
			slideCoords.update(($coords) => ({
				x: $coords.x + dx,
				y: $coords.y + dy,
			}));
		}

		onSlideMove && onSlideMove();
	}

	function handleMouseup() {
		if (Math.abs(slidingXDelta) > 0.5 * node.getBoundingClientRect().width) {
			onSlideEnd && onSlideEnd();
		} else {
			slideCoords.stiffness = 0.05;
			slideCoords.damping = 0.5;
			slideCoords.set({ x: 0, y: 0 });
		}

		x = 0;
		y = 0;
		slidingXDelta = 0;

		window.removeEventListener('touchmove', handleMousemove);
		window.removeEventListener('touchend', handleMouseup);

		if (trackMouse) {
			window.removeEventListener('mousemove', handleMousemove);
			window.removeEventListener('mouseup', handleMouseup);
		}
	}

	node.addEventListener('touchstart', handleMousedown);

	if (trackMouse) {
		node.addEventListener('mousedown', handleMousedown);
	}

	return {
		destroy() {
			node.removeEventListener('mousedown', handleMousedown);
		},
	};
}

type MaybeSlideTransitionParams = {
	duration?: number;
	axis?: 'x' | 'y';
	canSlide: () => boolean;
};

export const maybeSlideTransition = (
	node: HTMLElement,
	{ duration, canSlide }: MaybeSlideTransitionParams,
) => {
	if (!canSlide()) {
		return {
			duration: 0,
		};
	}

	const result = node.style.transform?.match(/translate3d\((-?\d+)px, .*\)/);
	const initialTranslation = result ? parseInt(result[1], 10) : 0;
	const initialTranslationPercent =
		(initialTranslation / node.getBoundingClientRect().width) * 100;

	return {
		duration,
		tick: (t: number) => {
			const newTranslation =
				initialTranslationPercent + (1 - t) * (100 - initialTranslationPercent);
			node.style.transform = `translate3d(${newTranslation}%, 0, 0)`;
		},
	};
};
