import { unwrapFunctionStore, format } from 'svelte-i18n';
import * as rssBackend from './backends';
import {
	authenticationValues as authenticationStore,
	reset as resetAuthData,
} from '../stores/auth';
import { options as optionsStore, type Options } from '../stores/options';
import { categories as categoriesStore, setActiveCategory } from '../stores/categories';
import {
	articles as articlesStore,
	updateStatus as updateArticleStatus,
	articlesToMarkAsRead as articlesToMarkAsReadStore,
	processingArticles,
	failedToProcessArticles,
	deleteArticle,
} from '../stores/articles';
import type { ArticleId, AuthenticationData, Category, CategoryId } from './backends/rss.types';
import { assertUnreachable } from './utils';
import { goto } from '$app/navigation';

let authenticationValues: AuthenticationData;
authenticationStore.subscribe((value) => {
	authenticationValues = value;
});

let options: Options | null = null;
optionsStore.subscribe((value) => {
	let mustUpdateArticlesForActiveCategory = false;
	if (
		options !== null &&
		(value.fetchDescription !== options.fetchDescription ||
			value.onlyUnread !== options.onlyUnread)
	) {
		mustUpdateArticlesForActiveCategory = true;
	}

	options = { ...value };

	if (mustUpdateArticlesForActiveCategory) {
		listArticlesForActiveCategory();
		mustUpdateArticlesForActiveCategory = false;
	}
});

let activeCategory: Category | null;
let categories: Category[] = [];
categoriesStore.subscribe((value) => {
	activeCategory = value.activeCategory;
	categories = value.categories;
});

let articlesToMarkAsRead: ArticleId[] = [];
articlesToMarkAsReadStore.subscribe((values) => (articlesToMarkAsRead = values));

const $format = unwrapFunctionStore(format);

export const login = (params: AuthenticationData): Promise<void> =>
	rssBackend.login(params).then(
		(token) =>
			authenticationStore.update((values) => ({
				...values,
				...params,
				isLoggedIn: true,
				token,
			})),
		(error) => {
			authenticationStore.update((values) => ({ ...values, isLoggedIn: false }));
			return Promise.reject(error);
		},
	);

export const logout = () => {
	return goto('/').then(() => {
		resetAuthData();
	});
};

export const listCategories = async (activeCategoryId: CategoryId) => {
	categoriesStore.update((state) => ({ ...state, isLoading: true }));

	const rawCategories = await rssBackend.listCategories(authenticationValues);

	categoriesStore.update((values) => ({
		...values,
		isLoading: false,
		categories: translateCategories(rawCategories),
	}));
	setActiveCategory(activeCategoryId);
};

const translateCategories = (categories: Category[]): Category[] =>
	categories.map((category) =>
		category.isSpecial ? { ...category, title: $format(category.title) } : category,
	);

export const transformCategoryUrlIdToCategoryId = (urlId: CategoryId): string =>
	rssBackend.transformCategoryUrlIdToCategoryId(authenticationValues, urlId);

export const listArticlesForActiveCategory = async () => {
	if (!activeCategory) {
		return;
	}

	articlesStore.update((state) => ({ ...state, isLoading: true }));
	const articles = await rssBackend.listArticles(authenticationValues, activeCategory.id, {
		fetchDescriptions: options?.fetchDescription,
		fetchMode: options?.onlyUnread ? 'unread' : 'all',
	});
	articlesStore.update((values) => ({ ...values, isLoading: false, articles }));
};

export const refreshCurrentCategory = async () => {
	await listArticlesForActiveCategory();
};

export const isLoggedIn = async (): Promise<void> => {
	if (!authenticationValues.host || !authenticationValues.token) {
		return Promise.reject();
	}

	const isLoggedIn = await rssBackend.isLoggedIn(authenticationValues);

	if (isLoggedIn) {
		authenticationStore.update((values) => ({ ...values, isLoggedIn }));
		return Promise.resolve();
	} else if (authenticationValues.username && authenticationValues.password) {
		return await login(authenticationValues);
	} else {
		authenticationStore.update((values) => ({ ...values, isLoggedIn: false }));
		return Promise.reject();
	}
};

export const markArticleAsRead = async (articleId: ArticleId): Promise<void> => {
	await changeArticleStatus([articleId], 'read');
};

export const markArticleAsReadAndDiscard = async (articleId: ArticleId): Promise<void> => {
	await markArticleAsRead(articleId);
	deleteArticle(articleId);
};

const changeArticleStatus = async (articleIds: ArticleId[], status: 'read' | 'unread') => {
	switch (status) {
		case 'read':
			await rssBackend.markArticleAsRead(authenticationValues, articleIds);
			break;
		case 'unread':
			await rssBackend.markArticleAsUnread(authenticationValues, articleIds);
			break;
		default:
			assertUnreachable(status);
			break;
	}

	updateArticleStatus(articleIds, status);

	await updateCountersForCategories();
};

export const markArticleAsUnread = async (articleId: ArticleId): Promise<void> => {
	await changeArticleStatus([articleId], 'unread');
};

export const purgeArticlesToBeRead = async (): Promise<void> => {
	const articlesToProcess = articlesToMarkAsRead;
	if (articlesToProcess.length === 0) {
		return;
	}

	processingArticles(articlesToProcess);
	try {
		await changeArticleStatus(articlesToProcess, 'read');
	} catch {
		failedToProcessArticles(articlesToProcess);
	}
};

const updateCountersForCategories = async () => {
	const rawCategories = await rssBackend.updateCountersForCategories(
		authenticationValues,
		categories,
	);

	categoriesStore.update((values) => ({
		...values,
		categories: translateCategories(rawCategories),
	}));
};
