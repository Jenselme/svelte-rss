type ScrollParams = {
	canRun: () => boolean;
	onScroll: () => void;
	onScrollEnd: () => void;
};

export const scroll = (node: HTMLElement, { canRun, onScroll, onScrollEnd }: ScrollParams) => {
	const handleScroll = () => {
		// We only do this on desktop. Swipe can be used on touch devices.
		if ('ontouchstart' in window) {
			return;
		} else if (!canRun()) {
			return;
		} else if (node.getBoundingClientRect().top < 0) {
			onScroll();
		}
	};

	const handleScrollEnd = () => {
		onScrollEnd();
	};

	window.addEventListener('scroll', handleScroll);
	window.addEventListener('scrollend', handleScrollEnd);

	return {
		destroy() {
			window.removeEventListener('scroll', handleScroll);
			window.removeEventListener('scrollend', handleScrollEnd);
		},
	};
};
