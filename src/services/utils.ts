export function assertUnreachable(value: never): never {
	throw new Error(
		`Statement should be unreachable with value: ${
			value === null
				? 'null'
				: value === undefined
				? 'undefined'
				: (value as object).toString()
		}`,
	);
}
