import { sanitizeDescription } from '../articles';
import type {
	Article,
	ArticleId,
	AuthenticationData,
	Category,
	CategoryId,
	Counter,
	ListArticlesOptions,
} from './rss.types';

export const login = (params: AuthenticationData): Promise<string> =>
	queryApi(params.host, { password: params.password, user: params.username, op: 'login' })
		.then((data) => data.session_id)
		.then((token) => token || Promise.reject(new Error('Invalid credentials')));

const buildUrl = (host: string) => new URL('api/', host);

const queryApi = async (host: string, payload: Record<string, string | boolean | number>) =>
	fetch(buildUrl(host), {
		method: 'POST',
		body: JSON.stringify(payload),
	})
		.then((response) => response.json())
		.then((data) => data.content);

enum TTRssFeedIds {
	all = -4,
	archived = 0,
	labels = -2,
	published = -2,
	stared = -1,
	unread = -3,
}

export const transformCategoryUrlIdToCategoryId = (urlId: CategoryId): string => {
	switch (urlId.toString().toLowerCase()) {
		case 'all':
			return TTRssFeedIds.all.toString();
		case 'unread':
			return TTRssFeedIds.unread.toString();
		case 'stared':
			return TTRssFeedIds.stared.toString();
		default:
			return urlId.toString();
	}
};

type RawCategory = { id: number; title: string };

export const listCategories = (authData: AuthenticationData): Promise<Category[]> => {
	const getCategories: Promise<RawCategory[]> = queryApi(authData.host, {
		sid: authData.token,
		op: 'getCategories',
	});
	const getUnreadCounts: Promise<Record<string, number>> = listCounters(authData);

	return Promise.all([getCategories, getUnreadCounts]).then(([categories, unreadCounts]) =>
		updateCategoriesWithCounter(categories, unreadCounts),
	);
};

export const updateCategoriesWithCounter = (
	categories: RawCategory[] | Category[],
	unreadCounts: Record<string, number>,
): Category[] => [
	{
		id: TTRssFeedIds.all.toString(),
		title: 'categories.special.all',
		unreadCount: unreadCounts['global-unread'] || 0,
		isSpecial: true,
	},
	{
		id: TTRssFeedIds.unread.toString(),
		title: 'categories.special.unread',
		unreadCount: unreadCounts[TTRssFeedIds.unread] || 0,
		isSpecial: true,
	},
	{
		id: TTRssFeedIds.stared.toString(),
		title: 'categories.special.favorites',
		unreadCount: unreadCounts[TTRssFeedIds.stared] || 0,
		isSpecial: true,
	},
	...removeSpecialCategories<RawCategory | Category>(categories).map(
		(rawCategory): Category => ({
			id: rawCategory.id.toString(),
			title: rawCategory.title,
			unreadCount: unreadCounts[rawCategory.id] || 0,
			isSpecial: false,
		}),
	),
];

const removeSpecialCategories = <T extends { id: number | string }>(categories: T[]): T[] =>
	categories.filter(
		(category) =>
			category.id.toString() !== TTRssFeedIds.all.toString() &&
			category.id.toString() !== TTRssFeedIds.unread.toString() &&
			category.id.toString() !== TTRssFeedIds.stared.toString(),
	);

export const listCounters = (authData: AuthenticationData): Promise<Record<string, number>> =>
	queryApi(authData.host, { sid: authData.token, op: 'getCounters' }).then((counters) =>
		counters
			.map(
				(counter: { id: number; counter: number }): Counter => ({
					categoryId: counter.id.toString(),
					unreadCount: counter.counter,
				}),
			)
			.reduce(
				(countsByCategory: Record<string, number>, counter: Counter) => ({
					...countsByCategory,
					[counter.categoryId]: counter.unreadCount,
				}),
				{},
			),
	);

enum ViewMode {
	all = 'all_articles',
	unread = 'unread',
}

interface TTRSSArticle {
	readonly author: string;
	readonly feed_id: number;
	readonly feed_title: string;
	readonly id: number;
	readonly unread: boolean;
	readonly marked: boolean;
	readonly link: string;
	readonly title: string;
	readonly content: string;
	readonly updated: number;
}

export const listArticles = (
	authData: AuthenticationData,
	categoryId: CategoryId,
	{ fetchDescriptions, fetchMode }: ListArticlesOptions,
): Promise<Article[]> => {
	const id = parseInt(transformCategoryUrlIdToCategoryId(categoryId), 10);

	if (!Number.isInteger(id)) {
		return Promise.reject(new Error('Invalid category id'));
	}

	return queryApi(authData.host, {
		sid: authData.token,
		op: 'getHeadlines',
		feed_id: id,
		is_cat: id >= 0,
		show_content: !!fetchDescriptions,
		view_mode: fetchMode === 'all' ? ViewMode.all : ViewMode.unread,
	}).then((rawArticles: TTRSSArticle[]) =>
		rawArticles.map((rawArticle) => ({
			author: rawArticle.author,
			feedId: rawArticle.feed_id,
			feedTitle: rawArticle.feed_title,
			id: rawArticle.id,
			isFavorite: rawArticle.marked,
			isRead: !rawArticle.unread,
			link: rawArticle.link,
			title: rawArticle.title,
			description: sanitizeDescription(rawArticle.content) || '',
			updatedAt: new Date(rawArticle.updated * 1000),
		})),
	);
};

export const isLoggedIn = (authData: AuthenticationData): Promise<boolean> =>
	queryApi(authData.host, { op: 'isLoggedIn', sid: authData.token })
		.then((data: { status: boolean }) => data.status)
		.then((isLoggedIn) => {
			if (isLoggedIn) {
				return true;
			}

			return false;
		});

enum TTRssArticleUpdateMode {
	setToFalse = 0,
	setToTrue = 1,
	toggle = 2,
}

enum TTRssUpdatableFields {
	starred = 0,
	published = 1,
	unread = 2,
	note = 3,
}

export const markArticleAsRead = (
	authData: AuthenticationData,
	articleIds: ArticleId[],
): Promise<void> =>
	updateArticle(
		authData,
		articleIds.join(','),
		TTRssUpdatableFields.unread,
		TTRssArticleUpdateMode.setToFalse,
	);

export const markArticleAsUnread = (
	authData: AuthenticationData,
	articleIds: ArticleId[],
): Promise<void> =>
	updateArticle(
		authData,
		articleIds.join(','),
		TTRssUpdatableFields.unread,
		TTRssArticleUpdateMode.setToTrue,
	);

const updateArticle = (
	authData: AuthenticationData,
	articleId: string,
	field: number,
	mode: number,
) =>
	queryApi(authData.host, {
		sid: authData.token,
		article_ids: articleId,
		field,
		mode,
		op: 'updateArticle',
	});
