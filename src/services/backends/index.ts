import type {
	Article,
	ArticleId,
	AuthenticationData,
	Category,
	CategoryId,
	ListArticlesOptions,
} from './rss.types';
import * as ttrss from './ttrss';

export const login = (params: AuthenticationData): Promise<string> => {
	switch (params.selectedBackend) {
		case 'TTRSS':
			return ttrss.login(params);
	}
};

export const listCategories = (authData: AuthenticationData): Promise<Category[]> => {
	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss.listCategories(authData);
	}
};

export const transformCategoryUrlIdToCategoryId = (
	authData: AuthenticationData,
	urlId: CategoryId,
): string => {
	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss.transformCategoryUrlIdToCategoryId(urlId);
	}
};

export const listArticles = (
	authData: AuthenticationData,
	categoryId: CategoryId,
	options: ListArticlesOptions,
): Promise<Article[]> => {
	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss.listArticles(authData, categoryId, options);
	}
};

export const isLoggedIn = (authData: AuthenticationData): Promise<boolean> => {
	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss.isLoggedIn(authData);
		default:
			return Promise.resolve(false);
	}
};

export const markArticleAsRead = (
	authData: AuthenticationData,
	articleIds: ArticleId[],
): Promise<void> => {
	if (articleIds.length === 0) {
		return Promise.resolve();
	}

	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss.markArticleAsRead(authData, articleIds);
	}
};

export const markArticleAsUnread = (
	authData: AuthenticationData,
	articleIds: ArticleId[],
): Promise<void> => {
	if (articleIds.length === 0) {
		return Promise.resolve();
	}

	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss.markArticleAsUnread(authData, articleIds);
	}
};

export const updateCountersForCategories = (
	authData: AuthenticationData,
	categories: Category[],
): Promise<Category[]> => {
	switch (authData.selectedBackend) {
		case 'TTRSS':
			return ttrss
				.listCounters(authData)
				.then((counters) => ttrss.updateCategoriesWithCounter(categories, counters));
	}
};
