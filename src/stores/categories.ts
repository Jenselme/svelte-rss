import { writable, type Writable } from 'svelte/store';
import type { Category, CategoryId } from '../services/backends/rss.types';
import { transformCategoryUrlIdToCategoryId } from '../services/connectedBackend';

type CategoriesStore = {
	categories: Category[];
	isLoading: boolean;
	activeCategory: Category | null;
};

export const categories: Writable<CategoriesStore> = writable({
	categories: [],
	isLoading: false,
	activeCategory: null,
});

export const setActiveCategory = (categoryId: CategoryId) => {
	const activeCategoryId = transformCategoryUrlIdToCategoryId(categoryId);
	categories.update((state) => ({
		...state,
		activeCategory:
			state.categories.find((category) => category.id === activeCategoryId) || null,
	}));
};
