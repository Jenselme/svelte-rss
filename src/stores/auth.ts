import { writable, type Writable } from 'svelte/store';
import type { AuthenticationData } from '../services/backends/rss.types';
import { loadAuthData, saveAuthData } from '../services/storage';

export const authenticationValues: Writable<AuthenticationData> = writable(
	loadAuthData() || {
		host: '',
		username: '',
		password: '',
		selectedBackend: 'TTRSS',
		token: '',
		isLoggedIn: false,
	},
);

authenticationValues.subscribe((value) => {
	saveAuthData(value);
});

export const reset = () =>
	authenticationValues.set({
		host: '',
		username: '',
		password: '',
		selectedBackend: 'TTRSS',
		token: '',
		isLoggedIn: false,
	});
