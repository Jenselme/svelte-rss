import { writable, type Writable } from 'svelte/store';
import type { Article, ArticleId } from '../services/backends/rss.types';

type ArticlesStore = {
	articles: Article[];
	isLoading: boolean;
};

export const articles: Writable<ArticlesStore> = writable({
	articles: [],
	isLoading: false,
});

export const articlesToMarkAsRead: Writable<ArticleId[]> = writable([]);

export const updateStatus = (articleIds: ArticleId[], status: 'read' | 'unread') =>
	articles.update((state) => ({
		...state,
		articles: state.articles.map((article) =>
			articleIds.includes(article.id) ? { ...article, isRead: status === 'read' } : article,
		),
	}));

export const deleteArticle = (articleId: ArticleId) =>
	articles.update((state) => ({
		...state,
		articles: state.articles.filter((article) => article.id !== articleId),
	}));

export const markArticleToBeRead = (articleId: ArticleId) => {
	articlesToMarkAsRead.update((state) =>
		state.includes(articleId) ? state : [...state, articleId],
	);
};

export const processingArticles = (processingArticles: ArticleId[]) => {
	articlesToMarkAsRead.update((state) =>
		state.filter((articleId) => !processingArticles.includes(articleId)),
	);
};

export const failedToProcessArticles = (articleIds: ArticleId[]) => {
	articlesToMarkAsRead.update((state) => [...state, ...articleIds]);
};
