import { writable, type Writable } from 'svelte/store';
import { loadOptionsData, saveOptions } from '../services/storage';

export type Options = {
	fetchDescription?: boolean;
	readOnScroll?: boolean;
	onlyUnread?: boolean;
};

export const options: Writable<Options> = writable(
	loadOptionsData() || {
		fetchDescription: true,
		readOnScroll: true,
	},
);

options.subscribe((value) => {
	saveOptions(value);
});
