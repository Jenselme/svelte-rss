import { sveltekit } from '@sveltejs/kit/vite';
import { defineConfig } from 'vitest/config';
import checker from 'vite-plugin-checker';
import viteTsconfigPaths from 'vite-tsconfig-paths';

export default defineConfig({
	plugins: [
		sveltekit(),
		checker({
			overlay: { initialIsOpen: false },
			typescript: true,
			eslint: {
				lintCommand: 'eslint src',
			},
		}),
		viteTsconfigPaths(),
	],
	test: {
		include: ['src/**/*.{test,spec}.{js,ts}'],
	},
});
